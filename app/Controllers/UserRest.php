<?php namespace App\Controllers;
use CodeIgniter\Controller;

use App\Models\UserModel;
use App\Models\CityModel;


class UserRest extends Controller{
  public function findAll(){
    $UserModel = new UserModel();
    return $this->response->setStatusCode(200)->setJSON($UserModel->findAll());
  }

  public function find($id){
    $UserModel = new UserModel();
    return $this->response->setStatusCode(200)->setJSON($UserModel->find($id));
  }

  public function create(){
    $user = $this->request->getJSON();
    $UserModel = new UserModel();
    $UserModel->insert($user);
    return $this->response->setStatusCode(200);
  }


  public function update(){
    $user = $this->request->getJSON();
    $UserModel = new UserModel();
    $UserModel->update($user->Id_User, $user);
    return $this->response->setStatusCode(200);
  }


  public function delete($id){

    $UserModel = new UserModel();
    $UserModel->delete($id);
    return $this->response->setStatusCode(200);
  }

}
