<?php namespace App\Models;
use CodeIgniter\Model;
/**
 *
 */
class CityModel extends Model{
  $protected $table = 'City';
  $protected $primarykey = 'Id_City';
  $protected $allfields = ['City'];

}
