<?php
namespace App\Models;
use CodeIgniter\Model;

class UserModel extends Model{
  protected $table = 'User';
  protected $primaryKey = 'Id_User';
  protected $allowedFields = ['Name','Phone','Id_City'];

}
