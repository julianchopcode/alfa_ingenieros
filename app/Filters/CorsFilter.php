<?php
namespace App\Filters;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class CorsFilter extends FilterInterface{

  public function before(RequestInterface $request){
    header('Acess-Control-Allow-Origin: *');
    header('Acess-Control-Allow-Methods: GET,POST,OPTIONS,PUT,DELTE');
  }
  public funtion after(RequestInterface $request, ResponseInterface $response){

  }
}
